package com.practice;

public class Application {
  public static void main(String[] args) {
    Application application = new Application();
    System.out.println("Specification version -->" + application.getClass().getPackage().getSpecificationVersion());
    System.out.println("Implementation version -->" + application.getClass().getPackage().getImplementationVersion());
  }
}
